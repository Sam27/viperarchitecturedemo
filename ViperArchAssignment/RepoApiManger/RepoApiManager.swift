//
//  RepoApiManager.swift
//  ViperArchAssignment
//
//  Created by Suzana on 18/05/18.
//  Copyright © 2018 Somesh. All rights reserved.
//

import UIKit

enum RepoDetailType {
    case RepoDetailLanguageEntity
    case RepoDetailContributorsEntity
    case RepoDetailBranchEntity
    case none
}

class RepoApiManager: NSObject {
    
    class func getRepoList(completion: @escaping (_ isSuccess: Bool,_ repoList: [RepoEntity]?) -> ())
    {
        let url = URL.init(string: "https://api.github.com/repositories")
        
        let task = URLSession.shared.dataTask(with: url!){(data, response, error) in
            guard let data = data else {
                completion(false, nil)
                print("Error :\(String(describing: error?.localizedDescription))")
                return
            }
            do{
                let repos = try JSONDecoder().decode([RepoEntity].self, from: data)
                completion(true, repos)
                print(repos)
            }
            catch{
                completion(false, nil)
                print(error)
            }
        }
        task.resume()
    }
    
    class func getAllRepoDetail(url:String, modelType:RepoDetailType , completion: @escaping ( _ isSuccess: Bool, _ repoList: Any?) -> ())
    {
        guard let url = URL.init(string: url) else{
            return
        }
        
        let task = URLSession.shared.dataTask(with: url){(data, response, error) in
            guard let data = data else {
                completion(false, nil)
                print("Error :\(String(describing: error?.localizedDescription))")
                return
            }
            do{
                switch modelType
                {
                case .RepoDetailBranchEntity:
                    let repos = try JSONDecoder().decode([RepoDetailBranchEntity].self, from: data)
                    completion(true, repos)
                    
                case .RepoDetailContributorsEntity:
                    let repos = try JSONDecoder().decode([RepoDetailContributorsEntity].self, from: data)
                    completion(true, repos)
                    
                case .RepoDetailLanguageEntity:
                    let repos = try JSONDecoder().decode([String: Int].self, from: data)
                    completion(true, repos)
                    
                case .none:
                    completion(false, nil)
                    
                }
            }
            catch{
                completion(false, nil)
                print(error)
            }
        }
        task.resume()
    }
}
