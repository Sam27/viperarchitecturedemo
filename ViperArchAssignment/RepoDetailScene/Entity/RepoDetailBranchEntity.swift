//
//  RepoDetailBranches.swift
//  ViperArchAssignment
//
//  Created by Suzana on 18/05/18.
//  Copyright © 2018 Somesh. All rights reserved.
//

import UIKit

struct RepoDetailBranchEntity: Codable {
    
    let name: String?
    
    enum CodingKeys:String,CodingKey {
        case name
    }
    
    init(from decoder:Decoder) throws
    {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }
}


