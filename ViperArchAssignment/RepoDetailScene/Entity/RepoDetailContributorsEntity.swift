//
//  RepoDetailContributor.swift
//  ViperArchAssignment
//
//  Created by Suzana on 18/05/18.
//  Copyright © 2018 Somesh. All rights reserved.
//

import Foundation
import UIKit

struct RepoDetailContributorsEntity: Codable {
    let loginName: String?
    let contributerType: String?
    let contributions: Int?
    
    enum CodingKeys:String, CodingKey {
        case loginName = "login"
        case contributerType = "type"
        case contributions = "contributions"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        loginName  = try values.decodeIfPresent(String.self, forKey: .loginName)
        contributerType = try values.decodeIfPresent(String.self, forKey: .contributerType)
        contributions = try values.decodeIfPresent(Int.self, forKey: .contributions)
    }
}


