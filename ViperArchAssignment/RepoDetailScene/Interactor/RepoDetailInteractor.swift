//
//  RepoDetailInteractor.swift
//  ViperArchAssignment
//
//  Created by Suzana on 18/05/18.
//  Copyright © 2018 Somesh. All rights reserved.
//

import Foundation

class RepoDetailInteractor: NSObject
{
    var entityDetailUrl: String?
    var delegate: RepoDetailInteractorToPresenterProtocol?
    
    func fetchRepoDetails(modelType:RepoDetailType)
    {
        let apiManager = RepoApiManager.getAllRepoDetail(url: entityDetailUrl!, modelType:modelType) { (isSuccess, repoDetail) in
            if(isSuccess)
            {
                if let repoDetails = repoDetail{
                    self.delegate?.repoDetailFetched(repos: repoDetails)
                }
            }
            else
            {
                self.delegate?.repoDetailFetchFailed()
            }
        }
    }
}
