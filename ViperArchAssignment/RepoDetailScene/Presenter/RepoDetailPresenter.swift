//
//  RepoDetailPresenter.swift
//  ViperArchAssignment
//
//  Created by Suzana on 18/05/18.
//  Copyright © 2018 Somesh. All rights reserved.
//

import Foundation
import UIKit

class RepoDetailPresenter: NSObject{
    
    var interactor: RepoDetailInteractor?
    var modelType: RepoDetailType = .none
    var delegate: RepoDetailPresenterToViewProtocol?
    
    var contriButionRepo: [RepoDetailContributorsEntity]?
    var branchRepo: [RepoDetailBranchEntity]?
    var languageRepo: [(String,Any)]?
    
    override init()
    {
        super.init()
    }
    
    func callRepoDetailInteractorToFetchDetails()
    {
        interactor?.fetchRepoDetails(modelType: modelType)
    }
    
    func getCountOfTableViewRows() -> Int
    {
        switch modelType {
        case .RepoDetailContributorsEntity:
            return contriButionRepo?.count ?? 0
        case .RepoDetailLanguageEntity:
            return languageRepo?.count ?? 0
        case .RepoDetailBranchEntity:
            return branchRepo?.count ?? 0
        default:
            return 0
        }
    }
    
    func getHeightForRepoDetailTableCell() -> CGFloat
    {
        switch modelType {
        case .RepoDetailContributorsEntity:
            return 70
        case .RepoDetailLanguageEntity:
            return 45
        case .RepoDetailBranchEntity:
            return 45
        default:
            return 0
        }
    }
    
    func getName(index: Int) -> String
    {
        var title = ""
        
        switch modelType {
        case .RepoDetailContributorsEntity:
            title = contriButionRepo?[index].loginName ?? ""
        case .RepoDetailLanguageEntity:
            title = languageRepo?[index].0 ?? ""
        case .RepoDetailBranchEntity:
            title = branchRepo?[index].name ?? ""
        default:
            title = ""
        }
        return title.uppercased()
    }
    
    func getContributorType(index: Int) -> String
    {
        var contriType:String = ""
        
        if modelType == .RepoDetailContributorsEntity
        {
            contriType = contriButionRepo?[index].contributerType ?? ""
        }
        if contriType != ""
        {
            contriType.append("  |  ")
        }
        return contriType
    }
    
    func getContributorCount(index: Int) -> String
    {
        var contriCount = ""
        
        if modelType == .RepoDetailContributorsEntity
        {
            let count = contriButionRepo?[index].contributions ?? 0
            contriCount = String(count)
            contriCount.append(" contributions")
        }
        return contriCount
    }
}

extension RepoDetailPresenter: RepoDetailInteractorToPresenterProtocol
{
    func repoDetailFetched(repos: Any) {
        switch modelType {
        case .RepoDetailContributorsEntity:
            if let contriRepo = repos as? [RepoDetailContributorsEntity]{
                contriButionRepo = contriRepo
            }
        case .RepoDetailLanguageEntity:
            if let langRepo = repos as? [String: Any]{
                var arr = [(String,Any)]()
                for (key, value) in langRepo {
                    let tuple = (key, value)
                    arr.append(tuple)
                }
                languageRepo = arr
            }
        case .RepoDetailBranchEntity:
            if let branchRepos = repos as? [RepoDetailBranchEntity]{
                branchRepo = branchRepos
            }
        case .none:
            break
        }
        
        DispatchQueue.main.async {
            self.delegate?.repoDetailFetched()
        }
    }
    
    func repoDetailFetchFailed() {
        self.delegate?.repoDetailFetchFailed()
    }
}
