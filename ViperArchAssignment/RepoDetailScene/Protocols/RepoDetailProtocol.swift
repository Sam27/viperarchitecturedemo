//
//  RepoDetailProtocol.swift
//  ViperArchAssignment
//
//  Created by Suzana on 18/05/18.
//  Copyright © 2018 Somesh. All rights reserved.
//

import Foundation

protocol RepoDetailInteractorToPresenterProtocol
{
    func repoDetailFetched(repos: Any)
    func repoDetailFetchFailed()
}

protocol RepoDetailPresenterToViewProtocol
{
    func repoDetailFetched()
    func repoDetailFetchFailed()
    
}

