//
//  RepoDetailRouter.swift
//  ViperArchAssignment
//
//  Created by Suzana on 18/05/18.
//  Copyright © 2018 Somesh. All rights reserved.
//

import UIKit

class RepoDetailRouter {
    class func getRepoDetailViewController(url:String, modelType:RepoDetailType) -> RepoDetailViewController
    {
        let repoListDetailVC = RepoDetailViewController.init(nibName: "RepoDetailViewController", bundle: nil)
        repoListDetailVC.presenter = RepoDetailPresenter()
        repoListDetailVC.presenter?.modelType = modelType
        repoListDetailVC.presenter?.delegate = repoListDetailVC
        repoListDetailVC.presenter?.interactor = RepoDetailInteractor()
        repoListDetailVC.presenter?.interactor?.entityDetailUrl = url
        repoListDetailVC.presenter?.interactor?.delegate = repoListDetailVC.presenter
        return repoListDetailVC
    }
}
