//
//  RepoDetailContributorTableViewCell.swift
//  ViperArchAssignment
//
//  Created by Suzana on 19/05/18.
//  Copyright © 2018 Somesh. All rights reserved.
//

import UIKit

class RepoDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var contributorType: UILabel!
    @IBOutlet weak var contributions: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        
    }
    
}
