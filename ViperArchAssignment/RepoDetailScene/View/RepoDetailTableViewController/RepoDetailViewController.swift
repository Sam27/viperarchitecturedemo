//
//  RepoDetailViewController.swift
//  ViperArchAssignment
//
//  Created by Suzana on 18/05/18.
//  Copyright © 2018 Somesh. All rights reserved.
//

import UIKit

class RepoDetailViewController: UIViewController {
    
    @IBOutlet weak var repoDetailTableView: UITableView!
    var presenter: RepoDetailPresenter?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        presenter?.callRepoDetailInteractorToFetchDetails()
        configureTableView()
    }
    
    func configureTableView()
    {
        let cellNib = UINib.init(nibName: "RepoDetailTableViewCell", bundle: nil)
        repoDetailTableView.register(cellNib, forCellReuseIdentifier:"RepoDetailTableViewCell")
        repoDetailTableView.delegate = self
        repoDetailTableView.dataSource = self
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}

extension RepoDetailViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:RepoDetailTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RepoDetailTableViewCell", for: indexPath) as! RepoDetailTableViewCell
        cell.title.text = presenter?.getName(index: indexPath.row)
        cell.contributions.text = presenter?.getContributorCount(index: indexPath.row)
        cell.contributorType.text = presenter?.getContributorType(index: indexPath.row)
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter!.getCountOfTableViewRows()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return presenter!.getHeightForRepoDetailTableCell()
    }
}

extension RepoDetailViewController: RepoDetailPresenterToViewProtocol
{
    func repoDetailFetched()
    {
        repoDetailTableView.reloadData()
    }
    
    func repoDetailFetchFailed()
    {
        let alert = UIAlertController(title: "Something went wrong.", message: "Please try again later.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
}
