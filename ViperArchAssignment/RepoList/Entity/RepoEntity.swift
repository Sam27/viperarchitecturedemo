//
//  RepoEntity.swift
//  ViperArchAssignment
//
//  Created by Suzana on 18/05/18.
//  Copyright © 2018 Somesh. All rights reserved.
//

import UIKit

struct RepoEntity: Codable {
    let name: String?
    let fullName: String?
    let contributorUrl: String?
    let languageUrl: String?
    let branchesUrl: String?
    
    enum CodingKeys:String,CodingKey {
        case name
        case fullName = "full_name"
        case contributorUrl = "contributors_url"
        case languageUrl = "languages_url"
        case branchesUrl = "branches_url"
    }
    
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        fullName = try values.decodeIfPresent(String.self, forKey: .fullName)
        contributorUrl = try values.decodeIfPresent(String.self, forKey: .contributorUrl)
        languageUrl = try values.decodeIfPresent(String.self, forKey: .languageUrl)
        
        if let newBranchUrl = try values.decodeIfPresent(String.self, forKey: .branchesUrl){
            let removeString = "{/branch}"
            let branchString  = newBranchUrl.replacingOccurrences(of: removeString, with: "")
            branchesUrl = branchString
        }
        else
        {
            branchesUrl = nil
        }
    }
}

