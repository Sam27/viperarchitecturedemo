//
//  RepoListPresenter.swift
//  ViperArchAssignment
//
//  Created by Suzana on 18/05/18.
//  Copyright © 2018 Somesh. All rights reserved.
//

import UIKit

class RepoListPresenter: NSObject
{
    var interactor: RepoListInteractor?
    var delegate: RepoPresenterToViewProtocol?
    var repoList = [RepoEntity]()
    
    override init() {
        super.init()
        
    }
    
    func callRepoInteractorToFetchRepos()
    {
        interactor?.fetchRepoList()
    }
    
    func getName(index: Int) -> String
    {
        let title = repoList[index].name ?? ""
        return title.uppercased()
    }
    
    func getFullName(index: Int) -> String
    {
        let fullName = repoList[index].fullName ?? ""
        return fullName
    }
}

extension RepoListPresenter: RepoInteractorToPresenterProtocol
{
    func repoListFetched(repos: [RepoEntity])
    {
        repoList = repos
        DispatchQueue.main.async {
            self.delegate?.repoListFetched()
        }
    }
    
    func repoListFetchFailed()
    {
        DispatchQueue.main.async {
            self.delegate?.repoListFetchFailed()
        }
    }
}

extension RepoListPresenter: RepoTableViewCellButtonProtocol
{
    func didClickonButtonOfType(type: ButtonType, index: Int)
    {
        var detailUrl = String()
        var modelType: RepoDetailType = .none
        
        switch type
        {
        case .contributors:
            if let contriUrl = repoList[index].contributorUrl {
                detailUrl = contriUrl
                modelType = .RepoDetailContributorsEntity
            }
        case .branches:
            if let branchUrl = repoList[index].branchesUrl {
                detailUrl = branchUrl
                modelType = .RepoDetailBranchEntity
            }
        case .languages:
            if let langUrl = repoList[index].languageUrl {
                detailUrl = langUrl
                modelType = .RepoDetailLanguageEntity
            }
        }
        
        let repoDetailRouter = RepoDetailRouter.getRepoDetailViewController(url:detailUrl,modelType: modelType)
        delegate?.presentRepoDetailViewController(detailVC: repoDetailRouter)
    }
}
