//
//  RepoListProtocol.swift
//  ViperArchAssignment
//
//  Created by Suzana on 18/05/18.
//  Copyright © 2018 Somesh. All rights reserved.
//

import UIKit

protocol RepoInteractorToPresenterProtocol
{
    func repoListFetched(repos: [RepoEntity])
    func repoListFetchFailed()
}

protocol RepoPresenterToViewProtocol
{
    func repoListFetched()
    func repoListFetchFailed()
    
    func presentRepoDetailViewController(detailVC: UIViewController)
}
