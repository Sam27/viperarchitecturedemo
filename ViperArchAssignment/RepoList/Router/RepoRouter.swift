//
//  File.swift
//  ViperArchAssignment
//
//  Created by Suzana on 18/05/18.
//  Copyright © 2018 Somesh. All rights reserved.
//

import UIKit

class RepoRouter: NSObject
{
    class func getRepoListViewController() -> UINavigationController{
        let repoListVC = RepoRouterViewController.init(nibName: "RepoRouterViewController", bundle: nil)
        repoListVC.presenter = RepoListPresenter()
        repoListVC.presenter?.delegate = repoListVC
        repoListVC.presenter?.interactor = RepoListInteractor()
        repoListVC.presenter?.interactor?.delegate = repoListVC.presenter
        
        let navVC = UINavigationController.init(rootViewController: repoListVC)
        navVC.navigationBar.topItem?.title = "Public Repositories"
        return navVC
    }
}
