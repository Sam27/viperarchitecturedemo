//
//  RepoRouterViewController.swift
//  ViperArchAssignment
//
//  Created by Suzana on 18/05/18.
//  Copyright © 2018 Somesh. All rights reserved.
//

import UIKit

class RepoRouterViewController: UIViewController {
    
    @IBOutlet weak var repoTableView: UITableView!
    var presenter: RepoListPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.callRepoInteractorToFetchRepos()
        configureTableView()
    }
    
    func configureTableView()
    {
        let cellNib = UINib.init(nibName: "RepoTableViewCell", bundle: nil)
        repoTableView.register(cellNib, forCellReuseIdentifier:"RepoTableViewCell")
        repoTableView.delegate = self
        repoTableView.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension RepoRouterViewController: RepoPresenterToViewProtocol
{
    func presentRepoDetailViewController(detailVC: UIViewController)
    {
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func repoListFetched() {
        repoTableView.reloadData()
    }
    
    func repoListFetchFailed() {
        let alert = UIAlertController(title: "Something went wrong.", message: "Please try again later.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
}

extension RepoRouterViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:RepoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RepoTableViewCell", for: indexPath) as! RepoTableViewCell
        cell.title.text = presenter?.getName(index: indexPath.row)
        cell.tag = indexPath.row
        cell.delegate = presenter
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.repoList.count ?? 0
    }
}


