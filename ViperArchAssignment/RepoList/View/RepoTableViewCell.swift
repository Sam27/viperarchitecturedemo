//
//  RepoTableViewCell.swift
//  ViperArchAssignment
//
//  Created by Suzana on 18/05/18.
//  Copyright © 2018 Somesh. All rights reserved.
//

import UIKit

protocol RepoTableViewCellButtonProtocol {
    func didClickonButtonOfType(type: ButtonType, index: Int)
}

enum ButtonType{
    case contributors
    case branches
    case languages
}

class RepoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    var delegate: RepoTableViewCellButtonProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    @IBAction func didClickLanguage(_ sender: UIButton)
    {
        delegate?.didClickonButtonOfType(type: .languages,index: tag)
    }
    
    @IBAction func didClickContributors(_ sender: UIButton) {
        delegate?.didClickonButtonOfType(type: .contributors,index: tag)
    }
    
    @IBAction func didClickBranches(_ sender: UIButton) {
        delegate?.didClickonButtonOfType(type: .branches,index: tag)
    }
}
